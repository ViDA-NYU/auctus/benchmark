#!/bin/bash

set -eux

cd "$(dirname "$(dirname "$0")")"

# Check Datamart exists
if [ ! -e datamart-benchmark ]; then
  git clone --recurse-submodules https://gitlab.com/ViDA-NYU/datamart/datamart.git datamart-benchmark
fi

if [ ! -e datamart-benchmark/.env ]; then
  cp datamart-benchmark/env.default datamart-benchmark/.env
fi

# Check Elasticsearch data exists
if [ ! -e datamart-benchmark/volumes/elasticsearch ]; then
  mkdir -p datamart-benchmark/volumes/elasticsearch
  tar -C datamart-benchmark/volumes/elasticsearch -Jxf index.20201124.socrata-worldbank.elasticsearch.tar.xz
fi

cd datamart-benchmark
. scripts/load_env.sh

# Set up base services
scripts/setup.sh
docker-compose up -d elasticsearch rabbitmq redis
sleep 5
docker-compose up -d lazo

# Build and re-start services
docker-compose build --build-arg version=v0.0 coordinator profiler apiserver test-discoverer
docker-compose up -d coordinator
sleep 2
docker-compose up -d --force-recreate profiler apiserver apilb

# Clear cache
docker exec -ti $(basename "$(pwd)")_coordinator_1 sh -c 'rm -rf /cache/*/*'
docker-compose exec redis redis-cli flushall

cd ..

sleep 15

# Run test queries
python test_queries.py
