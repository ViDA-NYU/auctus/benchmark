import csv
import logging
import os
import requests


logger = logging.getLogger('test_queries')


API_URL = os.environ['API_URL']


def do_query(name, req):
    response = requests.post(
        API_URL + '/search',
        json=req,
    )
    if response.status_code != 200:
        with open(f'{name}.csv', 'w') as fp:
            writer = csv.writer(fp)
            writer.writerow(['error,result'])
            writer.writerow([response.status_code, response.text])
        return
    results = response.json()['results']

    aug = any(
        r.get('augmentation', {}).get('type', 'none') != 'none'
        for r in results
    )

    with open(f'{name}.csv', 'w') as fp:
        writer = csv.writer(fp)
        if not aug:
            writer.writerow(['score', 'id', 'name'])
            for result in results:
                writer.writerow([
                    result['score'], result['id'], result['metadata']['name'],
                ])
        else:
            writer.writerow([
                'score', 'id', 'name',
                'aug_left_column', 'aug_right_column',
                'aug_temporal_resolution',
            ])
            for result in results:
                writer.writerow([
                    result['score'], result['id'], result['metadata']['name'],
                    ','.join(result['augmentation']['left_columns_names']),
                    ','.join(result['augmentation']['right_columns_names']),
                    result['augmentation'].get('temporal_resolution', ''),
                ])


def main():
    do_query(
        'taxi',
        {
            'keywords': 'taxi',
        },
    )


if __name__ == '__main__':
    main()
