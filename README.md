What's this?
============

This is a benchmark for evaluating search in Auctus, and view the effects of changes in code or configuration.

It includes a (partial) dump of the index of the system at a fixed date, so that changes in upstream data don't affect our tests.
